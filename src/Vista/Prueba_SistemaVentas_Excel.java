/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.SistemaVentas;
import Util.ExceptionUFPS;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class Prueba_SistemaVentas_Excel {
    
    public static void main(String[] args) {
        try {
            SistemaVentas mySistema=new SistemaVentas("src/Datos/vendedores.xls");
            System.out.println("Mi sistema tiene los vendedores: \n"+mySistema.toString());
            
            String resultado[]=mySistema.getPromedioVentas();
            for(String datos:resultado)
                System.out.println(datos);
            
            //Sólo del proceso PDF:
                    try {
                        System.out.println("Creando PDF...");
                        mySistema.crearInforme_PDF();
                        System.out.println("PDF creado...");
                    } catch (Exception ex) {
                        System.err.println("Error al crear el PDF");
                    }
            
        } catch (ExceptionUFPS ex) {
            System.err.println(ex.getMessage());
        } catch(IOException ex2)
        {
            System.err.println(ex2.getMessage());
        }
        
    }
    
}
