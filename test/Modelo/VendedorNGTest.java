/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Util.ExceptionUFPS;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author madar
 */
public class VendedorNGTest {
    
    private Vendedor vendedor_prueba;
    
    public VendedorNGTest() {
    }

    //1. Crear nuestro Escenario de prueba:
    
    public void escenario() throws ExceptionUFPS
    {
    this.vendedor_prueba=new Vendedor(8812,"madarme","345000,390000");
    //ideal--> vectorVentas={345000,390000}
    }
    
    
    public void escenario2() throws ExceptionUFPS
    {
    this.vendedor_prueba=new Vendedor(882,"njadarme","45000,39000,56000");
    //ideal--> vectorVentas={45000,39000,56000}
    }
    
    @Test
    public void testGetPromedioVentas() throws ExceptionUFPS
    {
    System.out.println("getPromedioVentas");
        //1. Invocar el escenario:
     this.escenario(); //Crear el objeto de prueba
     //2. Ideal:
     float total_ideal=(345000+390000)/2;
     float resultado=this.vendedor_prueba.getPromedioVentas();
        assertEquals(total_ideal,resultado);
    }
    
    @Test
    public void testGetCedula() throws ExceptionUFPS {
        System.out.println("getCedula");
        //1. Invocar el escenario:
        this.escenario(); //Crear el objeto de prueba
        //2. Ideal 
        long cedula_ideal=8812;
        //3. Ejecutar y evaluar la prueba:
        assertEquals(cedula_ideal, this.vendedor_prueba.getCedula());
        
    }
    
    @Test
    public void testGetCedula_escenario2() throws ExceptionUFPS {
        System.out.println("getCedula_escenario2");
        //1. Invocar el escenario 2:
        this.escenario2(); //Crear el objeto de prueba
        //2. Ideal 
        long cedula_ideal=882;
        //3. Ejecutar y evaluar la prueba:
        assertEquals(cedula_ideal, this.vendedor_prueba.getCedula());
        
    }
    

    @Test
    public void testGetVentas() throws ExceptionUFPS
    {
        System.out.println("getVentas2");
        //1. Invocar el escenario:
        this.escenario(); //Crear el objeto de prueba
        //2. Ideal -> //ideal--> vectorVentas={345000,390000}
        float vectorVentas_ideal[]={345000,390000};
        //3. Ejecutar y evaluar:
        //  el vector de ventas del vendedor tiene que ser igual al vector ideal
        //NO SIRVE--> if (vector_ventaIdeal==this.vendedor_prueba.getVentas())--> dir de memoria
        //Ejecutar:
        boolean prueba=this.sonIgualesVentas(vectorVentas_ideal, this.vendedor_prueba.getVentas());
        //Evaluar:
        assertEquals(prueba, true);
    }
    
    private boolean sonIgualesVentas(float v1[],float v2[])
    {
    if(v1.length!=v2.length)
        return false; // :(
    for(int i=0;i<v1.length;i++)
        {
            if(v1[i]!=v2[i])
                return false; // :(
        }
    return true; // :)
    }
   
}
